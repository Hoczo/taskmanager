//
//  ContentView.swift
//  TaskMenager
//
//  Created by Sławek Postawa on 01/04/2022.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Home()
            .padding()
            .preferredColorScheme(.light)
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
