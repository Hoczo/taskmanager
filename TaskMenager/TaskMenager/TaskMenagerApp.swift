//
//  TaskMenagerApp.swift
//  TaskMenager
//
//  Created by Sławek Postawa on 01/04/2022.
//

import SwiftUI

@main
struct TaskMenagerApp: App {
    
    @Environment(\.managedObjectContext) var managedObjectContext
    let persistenceController = PersistenceController.shared
    
    var body: some Scene {
        WindowGroup {
            ContentView()
                .environment(\.managedObjectContext, persistenceController.container.viewContext)
        }
    }
}
