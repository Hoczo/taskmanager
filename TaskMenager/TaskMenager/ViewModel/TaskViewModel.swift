//
//  SwiftUIView.swift
//  TaskMenager
//
//  Created by Sławek Postawa on 01/04/2022.
//

import SwiftUI

class TaskViewModel: ObservableObject {
    

    @Published var currentWeek: [Date] = []
    
    @Published var currentDay : Date = Date()
    
    @Published var filteredTask: [Task]?
    
    @Published var addnewTask: Bool = false
    
    @Published var editTask: Task?
    
    init() {
        fetchCurrentWeek()
    }
    
    
//    func filteredTodayTasks() {
//        DispatchQueue.global(qos: .userInteractive).async {
//            let calendar = Calendar.current
//            
//            let filtered = self.sortedTasks.filter{
//                return calendar.isDate($0.taskDate, inSameDayAs: self.currentDay) 
//            }
//                .sorted { task1, task2 in
//                    return task2.taskDate < task1.taskDate
//                }
//            
//            DispatchQueue.main.async {
//                withAnimation {
//                    self.filteredTask = filtered
//                }
//            }
//        }
//    }
    
    func fetchCurrentWeek() {
        let today = Date()
        let calenadr = Calendar.current
        
        let week = calenadr.dateInterval(of: .weekOfMonth, for: today)
        
        guard let firstWeekDay = week?.start else {
            return
        }
        
        (1...7).forEach { day in
            
            if let weekday = calenadr.date(byAdding: .day, value: day, to: firstWeekDay) {
                currentWeek.append(weekday )
            }
        }
    }
    
    func extractDate(date: Date, format: String) -> String{
        
        let formatter = DateFormatter()
        
        formatter.dateFormat = format
        
        return formatter.string(from: date)
        
        
    }
    
    func isToday(date: Date)-> Bool {
        let calendar = Calendar.current
        
        return calendar.isDate(currentDay, inSameDayAs: date)
    }
    
    func isCurrentHour(date: Date)->Bool {
         
        let calendar = Calendar.current
        
        let hour = calendar.component(.hour, from: date)
        let currentHour = calendar.component(.hour, from: Date())
        
        let isToday = calendar.isDateInToday(date)
        
        return (hour == currentHour && isToday)
    }
}
